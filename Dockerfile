#COMANDO PARA BUILD DA IMAGEM:
#docker build -t selenium-chrome-tester .

#COMANDO PADRÃO, QUE EXECUTA O "dotnet test" NA PASTA RAIZ DO VOLUME
#docker run --rm -v ~/Test_Auto:/app selenium-chrome-tester

#COMANDO PARA EXECUTAR OS TESTES COM FILTROS:
#docker run --rm -v ~/Test_Auto:/app selenium-chrome-tester --filter=T1_ItemsOnScreen

FROM mcr.microsoft.com/dotnet/core/sdk:3.1
#EXPOSE 4444

RUN apt-get update -y
RUN apt-get install -y unzip

#INSTALL CHROME
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt-get install -y ./google-chrome-stable_current_amd64.deb

#INSTALL CHROME-WEBDRIVER
RUN wget -N https://chromedriver.storage.googleapis.com/88.0.4324.96/chromedriver_linux64.zip -P ~/
RUN unzip ~/chromedriver_linux64.zip -d ~/

WORKDIR /app
ENTRYPOINT ["dotnet", "test"]
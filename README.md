### COMANDO PARA BUILD DA IMAGEM:

`docker build -t selenium-chrome-tester .`


### COMANDO PADRÃO, QUE EXECUTA O "dotnet test" NA PASTA RAIZ DO VOLUME

`docker run --rm -v ~/<Test_Project_Dir>:/app selenium-chrome-tester`


### COMANDO PARA EXECUTAR OS TESTES COM FILTROS:

`docker run --rm -v ~/<Test_Project_Dir>:/app selenium-chrome-tester --filter=T1_ItemsOnScreen`
